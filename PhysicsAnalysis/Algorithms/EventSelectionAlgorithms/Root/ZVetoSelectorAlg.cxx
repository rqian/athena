/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author Binbin Dong

#include "EventSelectionAlgorithms/ZVetoSelectorAlg.h"

namespace CP {

  ZVetoSelectorAlg::ZVetoSelectorAlg(const std::string &name, ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm(name, pSvcLocator)
  {}

  StatusCode ZVetoSelectorAlg::initialize() {
    ANA_CHECK(m_electronsHandle.initialize(m_systematicsList, SG::AllowEmpty));
    ANA_CHECK(m_electronSelection.initialize(m_systematicsList, m_electronsHandle, SG::AllowEmpty));
    ANA_CHECK(m_muonsHandle.initialize(m_systematicsList, SG::AllowEmpty));
    ANA_CHECK(m_muonSelection.initialize(m_systematicsList, m_muonsHandle, SG::AllowEmpty));
    ANA_CHECK(m_eventInfoHandle.initialize(m_systematicsList));
 
    ANA_CHECK(m_preselection.initialize(m_systematicsList, m_eventInfoHandle, SG::AllowEmpty));
    ANA_CHECK(m_decoration.initialize(m_systematicsList, m_eventInfoHandle));
    ANA_CHECK(m_systematicsList.initialize());
 
    return StatusCode::SUCCESS;
  }

  StatusCode ZVetoSelectorAlg::execute() {
    for (const auto &sys : m_systematicsList.systematicsVector()) {
      // retrieve the EventInfo
      const xAOD::EventInfo *evtInfo = nullptr;
      ANA_CHECK(m_eventInfoHandle.retrieve(evtInfo, sys));

      // default-decorate EventInfo
      m_decoration.setBool(*evtInfo, 0, sys);

      // check the preselection
      if (m_preselection && !m_preselection.getBool(*evtInfo, sys))
        continue;

      // retrieve the electron container
      const xAOD::ElectronContainer *electrons = nullptr;
      if (m_electronsHandle)
        ANA_CHECK(m_electronsHandle.retrieve(electrons, sys));
      // retrieve the muon container
      const xAOD::MuonContainer *muons = nullptr;
      if (m_muonsHandle)
        ANA_CHECK(m_muonsHandle.retrieve(muons, sys));

      // apply the requested selection
      bool pairInWindow = false;
      std::vector<const xAOD::Electron*> passingElectrons;
      std::vector<const xAOD::Muon*> passingMuons;

      if (m_electronsHandle) {
        for (const xAOD::Electron *el : *electrons) {
          if (!m_electronSelection || m_electronSelection.getBool(*el, sys)) {
            passingElectrons.push_back(el);
          }
        }
      }
      if (m_muonsHandle) {
        for (const xAOD::Muon *mu : *muons) {
          if (!m_muonSelection || m_muonSelection.getBool(*mu, sys)) {
            passingMuons.push_back(mu);
          }
        }
      }

      if (passingElectrons.size() >= 2) {
        for (size_t i = 0; i < passingElectrons.size() - 1; ++i) {
          const xAOD::Electron* firstElectron = passingElectrons[i];
          for (size_t j = i + 1; j < passingElectrons.size(); ++j) {
            const xAOD::Electron* secondElectron = passingElectrons[j];
            if (firstElectron->charge() != secondElectron->charge()){
              float mll = (firstElectron->p4() + secondElectron->p4()).M();
              if (mll >= m_mll_lower && mll <= m_mll_upper) pairInWindow = true;
            }
          }
        }
      }
      if (passingMuons.size() >= 2) {
        for (size_t i = 0; i < passingMuons.size() - 1; ++i) {
          const xAOD::Muon* firstMuon = passingMuons[i];
          for (size_t j = i + 1; j < passingMuons.size(); ++j) {
            const xAOD::Muon* secondMuon = passingMuons[j];
            if (firstMuon->charge() != secondMuon->charge()){
              float mll = (firstMuon->p4() + secondMuon->p4()).M();
              if (mll >= m_mll_lower && mll <= m_mll_upper) pairInWindow = true;
            }
          }
        }
      }
      bool decision = !pairInWindow;
      m_decoration.setBool(*evtInfo, decision, sys);
  
    }
    return StatusCode::SUCCESS;
  }
}